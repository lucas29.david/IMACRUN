#pragma once 

#include<iostream>

#include <GL/glew.h>
#include <stdlib.h>
#include "Image.hpp"

class Texture
{
public:
    Texture(GLenum TextureTarget, const std::string& FileName);
    // ~Texture();

    bool Load();

    void Bind(GLenum TextureUnit);

private:
    std::string _fileName;
    GLenum _textureTarget;
    GLuint _textureObj;
    std::unique_ptr<glimac::Image> _pImage;
};
