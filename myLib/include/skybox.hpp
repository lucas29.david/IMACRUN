#pragma once

#include <iostream>
#include <vector>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include "glm.hpp"
#include "stb_image.h"
#include "FilePath.hpp"
#include "Program.hpp"


struct SkyboxProgram {
    Program _Program;

    GLint uProjMatrix;
    GLint uMVMatrix;
    GLint uTexture;


    SkyboxProgram(const FilePath& applicationPath):
        _Program(loadProgram(applicationPath.dirPath() + "shaders/skybox.vs.glsl",
                              applicationPath.dirPath() + "shaders/skybox.fs.glsl")) {
        uProjMatrix = glGetUniformLocation(_Program.getGLId(), "uProjMatrix");
        uMVMatrix = glGetUniformLocation(_Program.getGLId(), "uMVMatrix");
        uTexture = glGetUniformLocation(_Program.getGLId(), "uTexture");
    }
};


class Skybox{
    private:
        unsigned int _VAO;
        unsigned int _VBO;
        unsigned int _IBO;
        // SkyboxProgram _program;
        unsigned int _cubemapTexture;
        std::vector<std::string> _faces;
        std::vector<uint32_t> _Indices; 
        std::vector<glm::vec3> _Vertices;
        unsigned int loadCubemap();
        void generer();
        
    public:
        Skybox();
        ~Skybox();
        void draw();

};


