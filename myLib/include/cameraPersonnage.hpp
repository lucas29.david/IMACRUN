#pragma once

#include <glm/glm.hpp>

class CameraPersonnage
{
private:
    glm::vec3 _posCam;
    glm::vec3 _pVise;
    glm::vec3 _up;
public:
    CameraPersonnage(float positionCamX);
    ~CameraPersonnage();
    void posCam(glm::vec3 posCam);
    void pVise(glm::vec3 pVise);

    glm::vec3 GetPosCam() const;
    glm::vec3 GetPVise() const;
    glm::vec3 GetUp() const;
};