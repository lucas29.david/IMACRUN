#pragma once

#include "glm.hpp"

class Matrix{
    public:
        Matrix();
        ~Matrix();
        glm::mat4 getModel();
        void setModel(glm::mat4 model);
        void setMVMatrix(glm::mat4 view);
        void setMVPMatrix(glm::mat4 proj);
        void setNormalMatrix();
        void setMatrix(glm::mat4 model,glm::mat4 view,glm::mat4 proj);
        glm::mat4 getMVMatrix();
        glm::mat4 getMVPMatrix();
        glm::mat4 getNormalMatrix();
    private:
        glm::mat4 _Model;
        glm::mat4 _MVMatrix;
        glm::mat4 _MVPMatrix;
        glm::mat4 _NormalMatrix;
};
