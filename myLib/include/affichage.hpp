#pragma once
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <vector>
#include "structureObjet.hpp"
#include "FilePath.hpp"
#include "Program.hpp" 
#include "parcours.hpp"
#include "Image.hpp"
#include "Matrix.hpp"

struct Object3dProgram {
    Program _Program;

    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    
    Object3dProgram(const FilePath& applicationPath):
        _Program(loadProgram(applicationPath.dirPath() + "shaders/objet3d.vs.glsl",
                            applicationPath.dirPath() + "shaders/objet3d.fs.glsl")) {
        uMVPMatrix = glGetUniformLocation(_Program.getGLId(), "uMVPMatrix");
        uMVMatrix = glGetUniformLocation(_Program.getGLId(), "uMVMatrix");
        uNormalMatrix = glGetUniformLocation(_Program.getGLId(), "uNormalMatrix");
    }
};

struct ObjectParcoursProgram {
    Program _Program;

    GLint uMVPMatrix;
    GLint uMVMatrix;
    GLint uNormalMatrix;
    GLint uTexture;


    ObjectParcoursProgram(const FilePath& applicationPath):
        _Program(loadProgram(applicationPath.dirPath() + "shaders/parcours.vs.glsl",
                            applicationPath.dirPath() + "shaders/parcours.fs.glsl")) {
        uMVPMatrix = glGetUniformLocation(_Program.getGLId(), "uMVPMatrix");
        uMVMatrix = glGetUniformLocation(_Program.getGLId(), "uMVMatrix");
        uNormalMatrix = glGetUniformLocation(_Program.getGLId(), "uNormalMatrix");
        uTexture = glGetUniformLocation(_Program.getGLId(), "uTexture");
    }
};


class Decors
{
private:
    GLuint _vao;
    GLuint _vbo;
    GLuint _ibo;

public:
    Decors();
    ~Decors();
    void vbo(std::vector<ShapeVertex> vertices);
    void vao();
    void ibo(std::vector<uint32_t> indices);
    void bindVao ();
    void debindVao();
    GLuint creerTexture(std::unique_ptr<glimac::Image> &img);
    void bindTexture(GLuint &tex);
    void debindTexture();
    void creerMur();
    void afficher(ObjectParcoursProgram &Program, GLuint &tex, Matrix &m);
    void creerSol();
    void afficheSol(ObjectParcoursProgram &Program, GLuint &tex, Matrix &m);
    void creerObstacle();
    void afficheObstacle(ObjectParcoursProgram &Program, GLuint &tex, Matrix &m);
    void creerArche();
    void afficheArche(ObjectParcoursProgram &Program, GLuint &tex, Matrix &m);
};

