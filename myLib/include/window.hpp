#pragma once

#include <SDL2/SDL.h>
#include <iostream>
#include <GL/glew.h>

class Window
{
private:
    SDL_Window* _window;
    SDL_GLContext _context;
    int _minor;
    int _major;
    int _nbdoublebuffer;
    int _depthsize;

    
    bool initSDL();
    bool initWindow();
    bool initContext();    
    bool initGLEW();
    void version();
    void buffer();
public:
    Window();
    ~Window();
    void Swap();
    bool init();
    SDL_Window* getWindow();


};
