#pragma once

#include <vector>
#include <fstream>
#include <iostream>
#include <string>

class Parcours
{
private:
    std::vector<std::vector<int>> _data;
    int _largeur;
    int _longueur;

public:
    Parcours();
    ~Parcours();
    std::vector<std::vector<int>> GetData() const;
    int Getlargeur() const;
    int Getlongueur() const;

    bool Lire(std::string &filename1,std::string &filename2);

    bool LireInfos(std::string &filename);
    bool LireMap(std::string &filename);
    bool IsVide(int i,int j);
    bool IsSol(int i,int j);
    bool IsMur(int i,int j);
    bool IsObstacle(int i,int j);
    bool IsArche(int i,int j);
    bool InParcours(int i,int j);

    void AfficherData();
};



