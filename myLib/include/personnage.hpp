#pragma once

#include "structureObjet.hpp"
#include "import3d.hpp"
#include "affichage.hpp"
#include "input.hpp"
#include "parcours.hpp"
#include <cmath>
#include "glm.hpp"
#include "cameraPersonnage.hpp"
#include "Matrix.hpp"
#include "bouteille.hpp"

class Personnage{
    public:
    	
        Personnage(const std::string &chemin);
        ~Personnage();
        void avancer();
        void Render();
        void placementInitial(float posInitiale);
        void gauche();
        void droite();
        void afficher(Object3dProgram &obj);
        void deplacer(Input const &input,Parcours &p,CameraPersonnage &camPerso);
        void rotationG();
        void rotationD();
        void glisser();
        void sauter();
        void setMatrix(glm::mat4 view,glm::mat4 proj);
        CameraPersonnage getCamPerso(CameraPersonnage &camPerso);
        void perdre(Parcours &p);
        void gagner(Parcours &p);
        bool attraper_bouteille(glm::vec2 coord,bool attrape);
        glm::vec2 position();
        int affichageHorizontal() ;
        int affichageVertical() ;
        glm::vec2 indicesV(Parcours &p); 
        glm::vec2 indicesH(Parcours &p); 
        bool colisionMurG(Parcours &p,float i,float j);
        bool colisionMurD(Parcours &p,float i,float j);
        bool virageProche(Parcours &p,float i,float j);
        void virage(Parcours &p,float i,float j);
        void afficheScore();
        bool gagne() const;
        bool perd() const;

        
    private:
        Matrix _matrix;
        float _vitesse;
        Mesh _mesh;
        const std::string _chemin;
        int _posLateral;
        int _axe;
        int _droitRotate;
        int _nbTourglisse;
        int _nbToursaute;
        bool _glisse;
        bool _saute;
        float _rotation;
        float _score;
        bool _gagner;
        bool _perdre;
};