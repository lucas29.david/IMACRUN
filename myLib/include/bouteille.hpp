#pragma once

#include "import3d.hpp"
#include "affichage.hpp"
#include <random>
#include "Matrix.hpp"
#include "glm.hpp"
#include <random>


class Bouteille{
    public:
        Bouteille(const std::string &chemin);
        ~Bouteille();
        // void placer(int i, int j);
        void Render();
        void placementInitial(float posInitiale);
        void setMatrix(glm::vec2 coord,glm::mat4 view,glm::mat4 proj);
        void setAttraper();
        void afficher(glm::vec2 coord,glm::mat4 view,glm::mat4 proj,Object3dProgram &obj);
        // glm::vec2 position();
        // void AfficherPos();
    private:
        Mesh _mesh;
        const std::string _chemin;
        Matrix _matrix;
        bool _attraper;

};
