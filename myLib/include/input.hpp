#pragma once

#include <SDL2/SDL.h>

enum class decision{
    jouer,
    attente,
    quitter,
};

class Input
{
    public:

    Input();
    ~Input();

    SDL_Event getEvenements() const;
    bool getTouche(const SDL_Scancode &touche) const;
    bool getBoutonSouris(const Uint8 &bouton) const;
    int getX() const;
    int getY() const;
    int getXRel() const;
    int getYRel() const;
    int getMolette() const;    
    bool GetTerminer() const;
    decision GetJouer() const;

    void updateEvenements();

    bool mouvementSouris() const;
    bool molette() const;
    void capturerPointeur(bool reponse) const;

    void testTerminer();
    void testJouer();
    void setTerminer(bool b);
    void setJouer(decision d);
    
    

    private:

    SDL_Event _evenements;

    bool _touches[SDL_NUM_SCANCODES];
    bool _boutonsSouris[8];

    int _x;
    int _y;
    int _xRel;
    int _yRel;
    
    int _molette;

    bool _terminer;
    decision _jouer;
};

