#pragma once
#include<vector>
#include<iostream>
#include<map>
#include "texture.hpp"
#include<cassert>
#include"structureObjet.hpp"
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>
#include <GL/glew.h>
#include "glm.hpp"

class Mesh
{
public:
    Mesh();

    ~Mesh();

    bool LoadMesh(const std::string& Filename);

    void Render();

private:
    bool InitFromScene(const aiScene* pScene, const std::string& Filename);
    void InitMesh(unsigned int Index, const aiMesh* paiMesh);
    bool InitMaterials(const aiScene* pScene, const std::string& Filename);
    void Clear();

#define INVALID_MATERIAL 0xFFFFFFFF
#define INVALID_VALUE 0xffffffff
#define SAFE_DELETE(p) if(p) { delete p; p=NULL; }

    struct MeshEntry {
        MeshEntry();

        ~MeshEntry();

        void Init(const std::vector<ShapeVertex>& Vertices,
        const std::vector<unsigned int>& Indices);

        GLuint VB;
        GLuint IB;
        unsigned int NumIndices;
        unsigned int MaterialIndex;
    };

    std::vector<MeshEntry> _Entries;
    std::vector<Texture*> _Textures;
};