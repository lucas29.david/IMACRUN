#pragma once

#include <glm/glm.hpp>
#include "input.hpp"

class Camera
{
private:

    glm::vec3 _PosCam;
    glm::vec3 _PVise;
    glm::vec3 _Up;
    glm::vec3 _Orientation;
    glm::vec3 _Left;

    float _phi;
    float _theta;

    float _sensi;
    float _vitesse;

public:

    /// \brief _____
	/// \param applicationPath : ______
    Camera();
    Camera(glm::vec3 PosCam);
    ~Camera();

    glm::vec3 GetPosCam() const;
    glm::vec3 GetPVise() const;
    glm::vec3 GetUp() const;
    glm::vec3 GetLeft() const;
    glm::vec3 GetOrientation() const;

    void deplacer(Input const &input);
    void orienter(int xRel, int yRel);
    void zoom(int scroll);
    void PosCamX(float a);
    void PosCamY(float a);
    void PosCamZ(float a);
    void phi(float a);
    void theta(float a);
    void modifVector();    
    void PVise();
    void Left();
    void Up();
    void Orientation();

};

