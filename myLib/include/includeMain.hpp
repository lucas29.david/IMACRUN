#pragma once

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <freetype2/freetype/config/ftheader.h>

#include "window.hpp"
#include "glm.hpp"
#include "FilePath.hpp"
#include "Program.hpp"
#include "camera.hpp"
#include "input.hpp"
#include "parcours.hpp"
#include "structureObjet.hpp"
#include "affichage.hpp"
#include "Image.hpp"
#include "stb_image.h"
#include "import3d.hpp"
#include "personnage.hpp"
#include "bouteille.hpp"
#include "cameraPersonnage.hpp"
#include "skybox.hpp"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <random>
#include "aleatoire.hpp"
#include <fstream>

#include "Matrix.hpp"
