#include "input.hpp"
#include "camera.hpp"
#include <iostream>

Input::Input() : _x(0), _y(0), _xRel(0), _yRel(0), _molette(0), _terminer(false),_jouer(decision::jouer)
{
    for(int i(0); i < SDL_NUM_SCANCODES; i++)
        _touches[i] = false;

    for(int i(0); i < 8; i++)
        _boutonsSouris[i] = false;
}

Input::~Input()
{}

SDL_Event Input::getEvenements() const{
    return _evenements;
}

bool Input::getTouche(const SDL_Scancode &touche) const{
    return _touches[touche];
}

bool Input::getBoutonSouris(const Uint8 &bouton) const
{
    return _boutonsSouris[bouton];
}

int Input::getMolette() const{
    return _molette;
}

int Input::getX() const
{
    return _x;
}

int Input::getY() const
{
    return _y;
}

int Input::getXRel() const
{
    return _xRel;
}

int Input::getYRel() const
{
    return _yRel;
}

bool Input::GetTerminer() const
{
    return _terminer;
}

decision Input::GetJouer() const
{
    return _jouer;
}

bool Input::molette() const{
    return !(_molette==0);
}

bool Input::mouvementSouris() const
{
    return !(_xRel == 0 && _yRel == 0);
}

void Input::capturerPointeur(bool reponse) const
{
    if(reponse)
        SDL_SetRelativeMouseMode(SDL_TRUE);

    else
        SDL_SetRelativeMouseMode(SDL_FALSE);
}

void Input::updateEvenements()
{
    _xRel = 0;
    _yRel = 0;
    _molette = 0;

    while(SDL_PollEvent(&_evenements))
    {
        switch(_evenements.type)
        {
            case SDL_KEYDOWN:
                _touches[_evenements.key.keysym.scancode] = true;
            break;

            case SDL_KEYUP:
                _touches[_evenements.key.keysym.scancode] = false;
            break;

            case SDL_MOUSEBUTTONDOWN:
                _boutonsSouris[_evenements.button.button] = true;
            break;

            case SDL_MOUSEBUTTONUP:
                _boutonsSouris[_evenements.button.button] = false;
            break;

            case SDL_MOUSEMOTION:
                _x = _evenements.motion.x;
                _y = _evenements.motion.y;

                _xRel = _evenements.motion.xrel;
                _yRel = _evenements.motion.yrel;
            break;
            
            case SDL_MOUSEWHEEL:
                _molette=_evenements.wheel.y;
            break;

            case SDL_WINDOWEVENT:
                if(_evenements.window.event == SDL_WINDOWEVENT_CLOSE)
                    _terminer = true;
            break;

            default:
            break;
        }
    }
}

void Input::testTerminer(){
    if (_touches[SDL_SCANCODE_ESCAPE]){
        _terminer=true;
        _jouer=decision::quitter;

    }
}

void Input::testJouer(){
    if(_touches[SDL_SCANCODE_Q]){
        _jouer=decision::jouer;
    }
    if(_touches[SDL_SCANCODE_ESCAPE]){
        _jouer=decision::quitter;
    }
}

void Input::setTerminer(bool b){
    _terminer=b;
}

void Input::setJouer(decision d){
    _jouer=d;
}