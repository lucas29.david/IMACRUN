#include "texture.hpp"


Texture::Texture(GLenum TextureTarget, const std::string& FileName)
{
    _textureTarget = TextureTarget;
    _fileName      = FileName;
    _pImage        = NULL;
}

// Texture::~Texture(){
    
// };

bool Texture::Load()
{
    _pImage = glimac::loadImage(_fileName);
    glGenTextures(1, &_textureObj);
    glBindTexture(_textureTarget, _textureObj);
    glTexImage2D(_textureTarget, 0, GL_RGBA, _pImage->getWidth(), _pImage->getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, _pImage->getPixels());
    glTexParameterf(_textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(_textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    if (_pImage==NULL)
        return false;
    return true;
}
void Texture::Bind(GLenum TextureUnit)
{
   glActiveTexture(TextureUnit);
   glBindTexture(_textureTarget, _textureObj);
}
