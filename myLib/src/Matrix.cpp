#include "Matrix.hpp"

Matrix::Matrix()
:_Model(1.f)
{}

Matrix::~Matrix(){}

glm::mat4 Matrix::getMVMatrix(){
    return _MVMatrix;
}
glm::mat4 Matrix::getMVPMatrix(){
    return _MVPMatrix;
}
glm::mat4 Matrix::getNormalMatrix(){
    return _NormalMatrix;
}

glm::mat4 Matrix::getModel(){
    return _Model;
}

void Matrix::setModel(glm::mat4 model){
    _Model=model;
}

void Matrix::setMVMatrix(glm::mat4 view){
    _MVMatrix = view * _Model;
}
void Matrix::setMVPMatrix(glm::mat4 proj){
    _MVPMatrix = proj * _MVMatrix;
}
void Matrix::setNormalMatrix(){
    _NormalMatrix=glm::transpose(glm::inverse(_MVMatrix));
}

void Matrix::setMatrix(glm::mat4 model,glm::mat4 view,glm::mat4 proj){
    setModel(model);
    setMVMatrix(view);
    setMVPMatrix(proj);
    setNormalMatrix();
}