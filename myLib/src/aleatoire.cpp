#include "aleatoire.hpp"

int aleatoire(int a,int b){
    std::random_device generator;
    std::uniform_int_distribution<int> distribution(a,b);
    return distribution(generator);
}