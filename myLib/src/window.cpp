#include "window.hpp"

Window::Window()
    :_minor(1),_major(3),_nbdoublebuffer(1),_depthsize(24)
{}

Window::~Window()
{}

bool Window::init(){
    if(!initSDL()){
        return false;   
    }
    if(!initWindow()){  
        return false;
    }
    if(!initContext()){  
        return false;
    }
    if(!initGLEW()){  
        return false;
    }
    // Version d'OpenGL
    version();
    // Double Buffer
    buffer();
    return true;
}

bool Window::initSDL(){
    if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << std::endl;
        SDL_Quit();
        return false;
    }
    return true;   
}

bool Window::initWindow(){
    uint flags=SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_FULLSCREEN;
    _window = SDL_CreateWindow("Test SDL 2.0", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1920, 1080, flags);
    if(_window == 0)
    {
        std::cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << std::endl;
        SDL_Quit();
        return false;
    }   
    return true;
}

bool Window::initContext(){
    _context = SDL_GL_CreateContext(_window);
    if(_context == 0)
    {
        std::cout << SDL_GetError() << std::endl;
        SDL_DestroyWindow(_window);
        SDL_Quit();
        return false;
    }
    return true;
}

bool Window::initGLEW(){
    GLenum glewInitError = glewInit();
    if(GLEW_OK != glewInitError) {
        std::cerr << glewGetErrorString(glewInitError) << std::endl;
        SDL_GL_DeleteContext(_context);
        SDL_DestroyWindow(_window);
        SDL_Quit();
        return false;
    }
    return true;
}

void Window::version(){
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, _minor);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, _major);
}
void Window::buffer()
{
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, _nbdoublebuffer);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, _depthsize);
}

void Window::Swap(){
    SDL_GL_SwapWindow(_window);    
}

SDL_Window* Window::getWindow(){
    return _window;
}