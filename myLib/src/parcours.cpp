#include "parcours.hpp"


Parcours::Parcours()
{
}

Parcours::~Parcours()
{
}

bool Parcours::LireInfos(std::string &filename){
    std::string line;
    std::ifstream file(filename,std::ifstream::in);
    if (!file.is_open()) {
        std::cerr << "Could not open the file - '" << filename << "'" << std::endl;
        return false;
    }
    std::getline(file, line);    
    _largeur=std::stoi(line);
    std::getline(file, line);  
    _longueur=std::stoi(line);
    file.close();
    return true;
}

bool Parcours::LireMap(std::string &filename){
    std::string valeur;
    std::vector<int> line;
    std::ifstream file(filename);
    if (!file.is_open()) {
        std::cerr << "Could not open the file - '" << filename << "'" << std::endl;
        return false;
    }
    for (int i = 0; i < _longueur; i++)
    {
        for (int j = 0; j < _largeur; j++)
        {
            std::getline(file, valeur);
            line.push_back(std::stoi(valeur));
        }
        _data.push_back(line);
        line.clear();
    }
    file.close();
    return true;    
}

bool Parcours::Lire(std::string &filename1,std::string &filename2){
    return (LireInfos(filename1)&&LireMap(filename2));
}

bool Parcours::IsVide(int i,int j){
    return (_data[i][j]==0);
}
bool Parcours::IsSol(int i,int j){
    return (_data[i][j]==1);
}
bool Parcours::IsMur(int i,int j){
    return (_data[i][j]==2);
}
bool Parcours::IsObstacle(int i,int j){
    return (_data[i][j]==3);
}
bool Parcours::IsArche(int i,int j){
    return (_data[i][j]==4);
}

bool Parcours::InParcours(int i,int j){
    return (!((j<0)|| (i<0)||(j>=_largeur)||(i>=_longueur)));
}

std::vector<std::vector<int>> Parcours::GetData() const{
    return _data;
}

int Parcours::Getlargeur() const{
    return _largeur;
}
int Parcours::Getlongueur() const{
    return _longueur;
}

void Parcours::AfficherData(){
    for (int i = 0; i < _longueur; i++)
    {
        std::cout << "(";
        for (int j = 0; j < _largeur; j++)
        {
            std::cout << _data[i][j];
            if(j<_largeur-1)
                std::cout << ", ";
        }
        std::cout << ")" << std::endl;
    }
}