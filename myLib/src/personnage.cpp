#include "personnage.hpp"

Personnage::Personnage(const std::string &chemin)
    :_vitesse(0.08),_chemin(chemin),_posLateral(0),_axe(0),_nbTourglisse(0),_nbToursaute(0),_rotation(0),_score(0),_gagner(false),_perdre(false)
{
    _mesh.LoadMesh(_chemin);
    _matrix.setModel(glm::mat4(1.f)),
    _matrix.setModel(glm::rotate(_matrix.getModel(),float(M_PI/2),glm::vec3(0,1,0)));
}

Personnage::~Personnage(){};

void Personnage::placementInitial(float posInitiale){
    _matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(-posInitiale-2.5f,0.5f,0)));
}

void Personnage::avancer(){
    if (_glisse)
        _matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(0,_vitesse*cos(_rotation),-_vitesse*sin(_rotation))));
    else
        _matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(0,0,_vitesse)));
}

void Personnage::setMatrix(glm::mat4 view,glm::mat4 proj){
    _matrix.setMVMatrix(view);
    _matrix.setMVPMatrix(proj);
    _matrix.setNormalMatrix();
}

void Personnage::afficher(Object3dProgram &obj){
    glUniformMatrix4fv(obj.uMVPMatrix,1,GL_FALSE,glm::value_ptr(_matrix.getMVPMatrix()));
    glUniformMatrix4fv(obj.uMVMatrix,1,GL_FALSE,glm::value_ptr(_matrix.getMVMatrix()));
    glUniformMatrix4fv(obj.uNormalMatrix,1,GL_FALSE,glm::value_ptr(_matrix.getNormalMatrix()));
    Render();
}


bool Personnage::colisionMurG(Parcours &p,float i,float j){
    if (_axe==0){
        return p.IsMur(int(i),int(j-1.f));
    }
    else if (_axe==1) {
        return p.IsMur(int(i+1.f),int(j));
    }
    else{
        return p.IsMur(int(i-1.f),int(j));
    }
}

bool Personnage::colisionMurD(Parcours &p,float i,float j){
    if (_axe==0){
        return p.IsMur(int(i),int(j+1.f));
    }
    else if (_axe==1) {
        return p.IsMur(int(i-1.f),int(j));
    }
    else{
        return p.IsMur(int(i+1.f),int(j));
    }
}


bool Personnage::virageProche(Parcours &p,float i,float j){
    if (_axe==0){
        i=int(i+5);
        j=int(j);
        if (i<p.Getlongueur())
            return(p.IsMur(i,j));
        else 
            return false;
    }
    else if (_axe==1){
        j=int(j+5);
        i=int(i);
        if (j<p.Getlargeur())
            return p.IsMur(i,j);
        else
            return false;
    }
    else{
        j=int(j-5);
        i=int(i);
        if (j>0)
            return p.IsMur(i,j);
        else
            return false;
    }
}

void Personnage::virage(Parcours &p,float i,float j){
    float a;
    if (_posLateral==1)
        a =2.5;
    if (_posLateral==0)
        a =1.5;
    if (_posLateral-=1)
        a =0.5;
    if (_axe==0){
        i=int(i+a);
        j=int(j);
        if (p.InParcours(i,j)){
            if(p.IsMur(i,j) && _droitRotate==2){
                rotationG();
            }
            if(p.IsMur(i,j) && _droitRotate==3){
                rotationD();
            }
        }
    }
    else if (_axe==1){
        j=int(j+a);
        i=int(i);
        if (p.InParcours(i,j)){
            if(p.IsMur(i,j) && _droitRotate==2){
                rotationG();
            }
            if(p.IsMur(i,j) && _droitRotate==3){
                rotationD();
            }
        }
    }
    else{
        j=int(j-a);
        i=int(i);
        if (p.InParcours(i,j)){
            if(p.IsMur(i,j) && _droitRotate==2){
                rotationG();
            }
            if(p.IsMur(i,j) && _droitRotate==3){
                rotationD();
            }
        }
    }
}

CameraPersonnage Personnage::getCamPerso(CameraPersonnage &camPerso){
    camPerso.pVise(glm::vec3(position().x, 0.5f,position().y));
    if (_axe==0)
        camPerso.posCam(glm::vec3(position().x-3.f, 5.5f,position().y));
    else if (_axe==1)
        camPerso.posCam(glm::vec3(position().x, 5.5f,position().y-3.f));
    else
        camPerso.posCam(glm::vec3(position().x, 5.5f,position().y+3.f));
    return camPerso;
}

void Personnage::glisser(){
    _nbTourglisse++;
    float a=0.1;
    //float t=0.02;
    if (_axe==0){
        if (_nbTourglisse<=10){
            _matrix.setModel(glm::rotate(_matrix.getModel(),-a,glm::vec3(1,0,0)));
            //_matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(0,-t*sin(_rotation),0)));
            _rotation-=a;

            }
        if (_nbTourglisse>30){
            
            //_matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(0,t*sin(_rotation),0)));
            _matrix.setModel(glm::rotate(_matrix.getModel(),a,glm::vec3(1,0,0)));
            _rotation-=a;
            }
    }
    if (_axe!=0){
        if (_nbTourglisse<=10){
            _matrix.setModel(glm::rotate(_matrix.getModel(),-a,glm::vec3(1,0,0)));
            //_matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(0,-t*sin(_rotation),0)));
            _rotation-=a;
            }
        if (_nbTourglisse>30){
            //_matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(0,t*sin(_rotation),0)));
            _matrix.setModel(glm::rotate(_matrix.getModel(),a,glm::vec3(1,0,0)));
            _rotation+=a;
            }  
    }
    if (_nbTourglisse==40){
        _nbTourglisse=0;
        _glisse=false;
        _rotation=0;
        }
}


void Personnage::deplacer(Input const &input,Parcours &p,CameraPersonnage &camPerso){
    float i=position().x;
    float j=position().y;
    if (virageProche(p,i,j)){
        _droitRotate=1;
    }
    if(input.getTouche(SDL_SCANCODE_X) && _droitRotate==1){
        _droitRotate=2;
    }
    if(input.getTouche(SDL_SCANCODE_C) && _droitRotate==1){
        _droitRotate=3;
    }
    if (_droitRotate>1)
        virage(p,i,j);
    if(input.getTouche(SDL_SCANCODE_SPACE)){
        _saute=true;
    }
    if(input.getTouche(SDL_SCANCODE_LSHIFT)){
        _glisse=true;
    }  
    if (_saute)
        sauter();
    if (_glisse)
        glisser();
    if(input.getTouche(SDL_SCANCODE_X) && !colisionMurG(p,i,j) && _droitRotate==0){
        gauche();
    }
    if(input.getTouche(SDL_SCANCODE_C) && !colisionMurD(p,i,j) && _droitRotate==0){
        droite();
    }
}

void Personnage::sauter(){
    _nbToursaute++;
    float t=0.4;
    if (_nbToursaute<=10)
        _matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(0,t,0)));
    if (_nbToursaute>10)
        _matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(0,-t,0)));    
    if (_nbToursaute==20){
        _nbToursaute=0;
        _saute=false;
        }
}

void Personnage::rotationG(){
    _droitRotate=0;
    if(_axe==0){
        _axe=-1;
    }
    else if(!_axe==0){
        _axe=0;
    }
    
    _matrix.setModel(glm::rotate(_matrix.getModel(),(float)M_PI/2,glm::vec3(0,1,0)));
}

void Personnage::rotationD(){
    _droitRotate=0;
    if(_axe==0){
        _axe=1;
    }
    else if(!_axe==0)
        _axe=0; 
    _matrix.setModel(glm::rotate(_matrix.getModel(),-(float)M_PI/2,glm::vec3(0,1,0)));
}

void Personnage::Render(){
    _mesh.Render();
}


void Personnage::gauche(){
    _matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(1,0,0)));
    _posLateral-=1;
}
void Personnage::droite(){
    _matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(-1,0,0)));

}


void Personnage::perdre(Parcours &p){
    
    if((p.IsMur(position().x,position().y) || p.IsObstacle(position().x,position().y) || p.IsArche(position().x,position().y) || p.IsVide(position().x,position().y)) && _matrix.getModel()[3][1]-0.5<abs(0.1)){
        _perdre=true;   
    }
}

void Personnage::gagner(Parcours &p){
    if (_axe==0){
        int pos=position().y;
        if (pos>=p.Getlargeur()){
            _gagner= true;
        }
    }
    else {
        int pos=position().x;
        if (pos>=(p.Getlongueur())||pos<=0){
            _gagner= true;
        }
    }
}

bool Personnage::attraper_bouteille(glm::vec2 coord,bool attrape){
    if ((attrape==false)&&(coord.x== int(position().x)) && (coord.y==int(position().y))){
        _score++;
        return true;
    }  
    else 
        return attrape;
}

glm::vec2 Personnage::position() {
    return(glm::vec2(_matrix.getModel()[3][0],_matrix.getModel()[3][2]));
}


int Personnage::affichageHorizontal() {
    if(_axe==0){
        return int(position().x);
    }
    else {
        return int(position().y);
    }
}


int Personnage::affichageVertical() {
    if(_axe==0){
        return int(position().y);
    }
    else {
        return int(position().x);
    }
}

glm::vec2 Personnage::indicesV(Parcours &p){
    int i1,i2;
    i1=affichageVertical()-40;
    i2=affichageVertical()+40;
    if (i1<0)
        i1=0;
    if ((i2>p.Getlargeur())&&(!_axe==0))
        i2=p.Getlargeur();
    if ((i2>p.Getlongueur())&&(_axe==0))
        i2=p.Getlongueur();
    return (glm::vec2(i1,i2));
}

glm::vec2 Personnage::indicesH(Parcours &p){
    int i1,i2;
    i1=affichageHorizontal()-40;
    i2=affichageHorizontal()+40;

    if (i1<0)
        i1=0;
    if ((i2>p.Getlargeur())&&(_axe==0))
        i2=p.Getlargeur();
    if ((i2>p.Getlongueur())&&(!_axe==0))
        i2=p.Getlongueur();
    return (glm::vec2(i1,i2));
}


void Personnage::afficheScore(){
    if(_gagner)
        std::cout<<"Vous êtes arrivé.e au bout avec "<<_score<<" bouteilles."<<std::endl;
    if(_perdre)
        std::cout<<"Vous n'êtes pas arrivé.e au bout, vous avez récolté "<<_score<<" bouteilles."<<std::endl;
}

bool Personnage::gagne() const{
    return _gagner;
}

bool Personnage::perd() const{
    return _perdre;
}
