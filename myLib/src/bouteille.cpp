#include "bouteille.hpp"

Bouteille::Bouteille(const std::string &chemin)    
:_chemin(chemin),_attraper(false)
{
    _mesh.LoadMesh(_chemin);
    _matrix.setModel(glm::mat4(1.f));

}
Bouteille::~Bouteille(){}

void Bouteille::Render(){
    _mesh.Render();
}

// void Bouteille::placer(int i, int j){
//     _matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(i,0,j)));

// }

// glm::vec2 Bouteille::position() {
//     return(glm::vec2(_matrix.getModel()[3][0],_matrix.getModel()[3][2]));
// }


void Bouteille::placementInitial(float posInitiale){
    _matrix.setModel(glm::translate(_matrix.getModel(),glm::vec3(-posInitiale-2.5f,0.5f,0)));
}

void Bouteille::setMatrix(glm::vec2 coord,glm::mat4 view,glm::mat4 proj)
{
    _matrix.setModel(glm::translate(glm::mat4(1.f),glm::vec3(coord.x+0.5,0,coord.y+0.5)));
    _matrix.setModel(glm::scale(_matrix.getModel(),glm::vec3(0.35,0.35,0.35)));
    _matrix.setMVMatrix(view);
    _matrix.setMVPMatrix(proj);
    _matrix.setNormalMatrix();
}


void Bouteille::afficher(glm::vec2 coord,glm::mat4 view,glm::mat4 proj,Object3dProgram &obj){
    setMatrix(coord,view,proj);
    glUniformMatrix4fv(obj.uMVPMatrix,1,GL_FALSE,glm::value_ptr(_matrix.getMVPMatrix()));
    glUniformMatrix4fv(obj.uMVMatrix,1,GL_FALSE,glm::value_ptr(_matrix.getMVMatrix()));
    glUniformMatrix4fv(obj.uNormalMatrix,1,GL_FALSE,glm::value_ptr(_matrix.getNormalMatrix()));
}

// void Bouteille::AfficherPos(){
//     std::cout << "("<< position().x<< ", "<< position().y << ")" << std::endl;
// }