#include "cameraPersonnage.hpp"

CameraPersonnage::CameraPersonnage(float positionCamX)
:_posCam(glm::vec3(-3.f,4.f,positionCamX+2.5f)), _pVise(glm::vec3(0,0.5f,positionCamX+2.5f)), _up(0,1,0)
{}

CameraPersonnage::~CameraPersonnage()
{}

void CameraPersonnage::posCam(glm::vec3 posCam){
    _posCam=posCam;
}

void CameraPersonnage::pVise(glm::vec3 pVise){
    _pVise=pVise;
}

glm::vec3 CameraPersonnage::GetPosCam() const{
    return _posCam;
}

glm::vec3 CameraPersonnage::GetPVise() const{
    return _pVise;
}

glm::vec3 CameraPersonnage::GetUp() const{
    return _up;
}