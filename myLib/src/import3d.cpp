#include"import3d.hpp"

Mesh::MeshEntry::MeshEntry()
{
    VB = INVALID_VALUE;
    IB = INVALID_VALUE;
    NumIndices  = 0;
    MaterialIndex = INVALID_MATERIAL;
};

Mesh::MeshEntry::~MeshEntry()
{
    if (VB != INVALID_VALUE)
    {
        glDeleteBuffers(1, &VB);
    }

    if (IB != INVALID_VALUE)
    {
        glDeleteBuffers(1, &IB);
    }
}

void Mesh::MeshEntry::Init(const std::vector<ShapeVertex>& Vertices,
                          const std::vector<unsigned int>& Indices)
{
    NumIndices = Indices.size();

    glGenBuffers(1, &VB);
  	glBindBuffer(GL_ARRAY_BUFFER, VB);
	glBufferData(GL_ARRAY_BUFFER, sizeof(ShapeVertex) * Vertices.size(), &Vertices[0], GL_STATIC_DRAW);

    glGenBuffers(1, &IB);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IB);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * NumIndices, &Indices[0], GL_STATIC_DRAW);
}


Mesh::Mesh()
{
}


Mesh::~Mesh()
{
    Clear();
}


void Mesh::Clear()
{
    for (unsigned int i = 0 ; i < _Textures.size() ; i++) {
        SAFE_DELETE(_Textures[i]);
    }
}




bool Mesh::LoadMesh(const std::string& Filename)
{
    // Release the previously loaded mesh (if it exists)
    Clear();

    bool Ret = false;
    Assimp::Importer Importer;

    const aiScene* pScene = Importer.ReadFile(Filename.c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);

    if (pScene) {
        Ret = InitFromScene(pScene, Filename);
    }
    else {
        std::cout << Filename.c_str() << Importer.GetErrorString() << std::endl;
    }

    return Ret;
}


bool Mesh::InitFromScene(const aiScene* pScene, const std::string& Filename)
{
    _Entries.resize(pScene->mNumMeshes);
    _Textures.resize(pScene->mNumMaterials);

    // Initialise les maillages de la scène, un par un
    for (unsigned int i = 0 ; i < _Entries.size() ; i++) {
        const aiMesh* paiMesh = pScene->mMeshes[i];
        InitMesh(i, paiMesh);
    }

    return InitMaterials(pScene, Filename);
}


void Mesh::InitMesh(unsigned int Index, const aiMesh* paiMesh)
{
    _Entries[Index].MaterialIndex = paiMesh->mMaterialIndex;

    std::vector<ShapeVertex> Vertices;
    std::vector<unsigned int> Indices;
    const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

    for (unsigned int i = 0 ; i < paiMesh->mNumVertices ; i++) {
        const aiVector3D* pPos = &(paiMesh->mVertices[i]);
        const aiVector3D* pNormal = &(paiMesh->mNormals[i]);
        const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &Zero3D;

        ShapeVertex v(glm::vec3(pPos->x, pPos->y, pPos->z),
                      glm::vec3(pNormal->x, pNormal->y, pNormal->z),
                      glm::vec2(pTexCoord->x, pTexCoord->y));

        Vertices.push_back(v);
    }
    for (unsigned int i = 0 ; i < paiMesh->mNumFaces ; i++) {
        const aiFace& Face = paiMesh->mFaces[i];
        assert(Face.mNumIndices == 3);
        Indices.push_back(Face.mIndices[0]);
        Indices.push_back(Face.mIndices[1]);
        Indices.push_back(Face.mIndices[2]);
    }
    _Entries[Index].Init(Vertices, Indices);
}


bool Mesh::InitMaterials(const aiScene* pScene, const std::string& Filename)
{
    bool Ret;
    for (unsigned int i = 0 ; i < pScene->mNumMaterials ; i++) {
        const aiMaterial* pMaterial = pScene->mMaterials[i];
        _Textures[i] = NULL;
        if (pMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
            aiString Path;

            if (pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &Path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS) {
                std::string TextureName = Path.data;
                std::string Dir="..assets/textures/";
                std::string FullPath = Dir + TextureName;
                _Textures[i] = new Texture(GL_TEXTURE_2D, FullPath.c_str());

                if (!_Textures[i]->Load()) {
                    printf("Error loading texture '%s'\n", FullPath.c_str());
                    delete _Textures[i];
                    _Textures[i] = NULL;
                    Ret = false;
                }
            }
        }
       if (!_Textures[i]) {
          _Textures[i] = new Texture(GL_TEXTURE_2D, "../assets/textures/triforce.png");
          Ret = _Textures[i]->Load();
       }
    }

    return Ret;
}

void Mesh::Render()
{
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    for (unsigned int i = 0 ; i < _Entries.size() ; i++) {
        glBindBuffer(GL_ARRAY_BUFFER, _Entries[i].VB);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), 0);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)12);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(ShapeVertex), (const GLvoid*)20);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _Entries[i].IB);

        const unsigned int MaterialIndex = _Entries[i].MaterialIndex;

        if (MaterialIndex < _Textures.size() && _Textures[MaterialIndex]) {
            _Textures[MaterialIndex]=new Texture(GL_TEXTURE_2D,"../assets/textures/triforce.png");
            _Textures[MaterialIndex]->Bind(GL_TEXTURE0);
        }

        glDrawElements(GL_TRIANGLES, _Entries[i].NumIndices, GL_UNSIGNED_INT, 0);
    }

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}

