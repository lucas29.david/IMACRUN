#include <SDL2/SDL.h>
#include "affichage.hpp"
#include "structureObjet.hpp"
#include <vector>


Decors::Decors()
{

}

Decors::~Decors()
{
    glDeleteBuffers(1,&_ibo);
    glDeleteVertexArrays(1,&_vao);
}

void Decors::vbo(std::vector<ShapeVertex> vertices){
    glGenBuffers(1, &_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, _vbo); 
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(ShapeVertex), vertices.data(), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Decors::vao(){//à optimiser
    glGenVertexArrays(1,&_vao);
    glBindVertexArray(_vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,_ibo);
    const GLuint VERTEX_ATTR_POSITION=0;
    const GLuint VERTEX_ATTR_COLOR=1;
    const GLuint VERTEX_ATTR_TEXTURE=2;
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION);
    glEnableVertexAttribArray(VERTEX_ATTR_COLOR);
    glEnableVertexAttribArray(VERTEX_ATTR_TEXTURE);
    glBindBuffer(GL_ARRAY_BUFFER,_vbo);
    glVertexAttribPointer(VERTEX_ATTR_POSITION,3,GL_FLOAT,GL_FALSE,sizeof(ShapeVertex),(const void*) offsetof(ShapeVertex,position));
    glVertexAttribPointer(VERTEX_ATTR_COLOR,3,GL_FLOAT,GL_FALSE,sizeof(ShapeVertex),(const void*) offsetof(ShapeVertex,normal));
    glVertexAttribPointer(VERTEX_ATTR_TEXTURE,2,GL_FLOAT,GL_FALSE,sizeof(ShapeVertex),(const void*) offsetof(ShapeVertex,texCoords));
    glBindBuffer(GL_ARRAY_BUFFER,0);
    glBindVertexArray(0);
}

void Decors::ibo(std::vector<uint32_t> indices){
    glGenBuffers(1,&_ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,_ibo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,indices.size()*sizeof(ShapeVertex),indices.data(),GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
}

void Decors::bindVao(){
    glBindVertexArray(_vao);
}

void Decors::debindVao(){
    glBindVertexArray(0);
}

GLuint Decors::creerTexture(std::unique_ptr<glimac::Image> &img){
    GLuint tex;
    glGenTextures(1,&tex);
    glBindTexture(GL_TEXTURE_2D,tex);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,img->getWidth(),img->getHeight(),0,GL_RGBA,GL_FLOAT,img->getPixels());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D,0);
    return tex;
}

void Decors::bindTexture(GLuint &tex){
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tex); 
}

void Decors::debindTexture(){
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Decors::creerMur(){
    std::vector<ShapeVertex> vertices={
        ShapeVertex(glm::vec3(0,0,0),glm::vec3(1,0,0),glm::vec2(0,1)),
        ShapeVertex(glm::vec3(1,0,0),glm::vec3(0,2,0),glm::vec2(1,1)),
        ShapeVertex(glm::vec3(1,1,0),glm::vec3(0,0,1),glm::vec2(1,0)),
        ShapeVertex(glm::vec3(0,1,0),glm::vec3(0,2,0),glm::vec2(0,0)),
        ShapeVertex(glm::vec3(0,0,1),glm::vec3(1,0,0),glm::vec2(0,0)),
        ShapeVertex(glm::vec3(1,0,1),glm::vec3(0,2,0),glm::vec2(1,0)),
        ShapeVertex(glm::vec3(1,1,1),glm::vec3(0,0,1),glm::vec2(1,1)),
        ShapeVertex(glm::vec3(0,1,1),glm::vec3(0,2,0),glm::vec2(0,1)),
        };
        
    vbo(vertices);
    std::vector<uint32_t> indices = {
        0, 1, 2, 0, 2, 3,
        1, 5, 6, 1, 6, 2,
        5, 4, 7, 5, 7, 6,
        4, 0, 3, 4, 3, 7,
        0, 1, 5, 0, 5, 4,
        3, 2, 6, 3, 6, 7 
    };
    ibo(indices);
    vao();
}

void Decors::afficher(ObjectParcoursProgram &Program, GLuint &tex, Matrix &m){
    bindVao();
    Program._Program.use();
    bindTexture(tex);
    glUniformMatrix4fv(Program.uMVPMatrix,1,GL_FALSE,glm::value_ptr(m.getMVPMatrix()));
    glUniformMatrix4fv(Program.uMVMatrix,1,GL_FALSE,glm::value_ptr(m.getMVMatrix()));
    glUniformMatrix4fv(Program.uNormalMatrix,1,GL_FALSE,glm::value_ptr(m.getNormalMatrix()));
    glUniform1i(Program.uTexture,0);
    glDrawElements(GL_TRIANGLES, 12*3, GL_UNSIGNED_INT, 0);
    debindTexture();
    debindVao();

}

void Decors::creerSol(){
    std::vector<ShapeVertex> vertices={
        ShapeVertex(glm::vec3(0,0,0),glm::vec3(1,0,0),glm::vec2(0,1)),
        ShapeVertex(glm::vec3(1,0,0),glm::vec3(0,1,0),glm::vec2(1,1)),
        ShapeVertex(glm::vec3(1,0.2,0),glm::vec3(0,0,1),glm::vec2(1,0)),
        ShapeVertex(glm::vec3(0,0.2,0),glm::vec3(0,1,0),glm::vec2(0,0)),
        ShapeVertex(glm::vec3(0,0,1),glm::vec3(1,0,0),glm::vec2(0,0)),
        ShapeVertex(glm::vec3(1,0,1),glm::vec3(0,1,0),glm::vec2(1,0)),
        ShapeVertex(glm::vec3(1,0.2,1),glm::vec3(0,0,1),glm::vec2(1,1)),
        ShapeVertex(glm::vec3(0,0.2,1),glm::vec3(0,1,0),glm::vec2(0,1)),
        };
        
    vbo(vertices);
    std::vector<uint32_t> indices = {
        0, 1, 2, 0, 2, 3,
        1, 5, 6, 1, 6, 2,
        5, 4, 7, 5, 7, 6,
        4, 0, 3, 4, 3, 7,
        0, 1, 5, 0, 5, 4,
        3, 2, 6, 3, 6, 7 
    };
    ibo(indices);
    vao();
}

void Decors::afficheSol(ObjectParcoursProgram &Program, GLuint &tex, Matrix &m){
    bindVao();

    Program._Program.use();
    bindTexture(tex);

    glUniform1i(Program.uTexture,0);
    glDrawElements(GL_TRIANGLES, 12*3, GL_UNSIGNED_INT, 0);
}

void Decors::creerObstacle(){
    std::vector<ShapeVertex> vertices={
        ShapeVertex(glm::vec3(0,0,0),glm::vec3(1,0,0),glm::vec2(0,1)),
        ShapeVertex(glm::vec3(1,0,0),glm::vec3(0,2,0),glm::vec2(1,1)),
        ShapeVertex(glm::vec3(1,1,0),glm::vec3(0,0,1),glm::vec2(1,0)),
        ShapeVertex(glm::vec3(0,1,0),glm::vec3(0,2,0),glm::vec2(0,0)),
        ShapeVertex(glm::vec3(0,0,1),glm::vec3(1,0,0),glm::vec2(0,0)),
        ShapeVertex(glm::vec3(1,0,1),glm::vec3(0,2,0),glm::vec2(1,0)),
        ShapeVertex(glm::vec3(1,1,1),glm::vec3(0,0,1),glm::vec2(1,1)),
        ShapeVertex(glm::vec3(0,1,1),glm::vec3(0,2,0),glm::vec2(0,1)),
        };
        
    vbo(vertices);
    std::vector<uint32_t> indices = {
        0, 1, 2, 0, 2, 3,
        1, 5, 6, 1, 6, 2,
        5, 4, 7, 5, 7, 6,
        4, 0, 3, 4, 3, 7,
        0, 1, 5, 0, 5, 4,
        3, 2, 6, 3, 6, 7 
    };
    ibo(indices);
    vao();
}

void Decors::afficheObstacle(ObjectParcoursProgram &Program, GLuint &tex, Matrix &m){
    bindVao();

    Program._Program.use();
    bindTexture(tex);

    glUniform1i(Program.uTexture,0);
    glDrawElements(GL_TRIANGLES, 12*3, GL_UNSIGNED_INT, 0);

}

void Decors::creerArche(){
    std::vector<ShapeVertex> vertices={
        ShapeVertex(glm::vec3(0,1,0),glm::vec3(1,0,0),glm::vec2(0,1)),
        ShapeVertex(glm::vec3(1,1,0),glm::vec3(0,1,0),glm::vec2(1,1)),
        ShapeVertex(glm::vec3(1,2,0),glm::vec3(0,0,1),glm::vec2(1,0)),
        ShapeVertex(glm::vec3(0,2,0),glm::vec3(0,1,0),glm::vec2(0,0)),
        ShapeVertex(glm::vec3(0,1,1),glm::vec3(1,0,0),glm::vec2(0,0)),
        ShapeVertex(glm::vec3(1,1,1),glm::vec3(0,1,0),glm::vec2(1,0)),
        ShapeVertex(glm::vec3(1,2,1),glm::vec3(0,0,1),glm::vec2(1,1)),
        ShapeVertex(glm::vec3(0,2,1),glm::vec3(0,1,0),glm::vec2(0,1)),
        };
        
    vbo(vertices);
    std::vector<uint32_t> indices = {
        0, 1, 2, 0, 2, 3,
        1, 5, 6, 1, 6, 2,
        5, 4, 7, 5, 7, 6,
        4, 0, 3, 4, 3, 7,
        0, 1, 5, 0, 5, 4,
        3, 2, 6, 3, 6, 7 
    };
    ibo(indices);
    vao();
}

void Decors::afficheArche(ObjectParcoursProgram &Program, GLuint &tex, Matrix &m){
    bindVao();
    Program._Program.use();
    bindTexture(tex);
    glUniform1i(Program.uTexture,0);
    glDrawElements(GL_TRIANGLES, 12*3, GL_UNSIGNED_INT, 0);
}
