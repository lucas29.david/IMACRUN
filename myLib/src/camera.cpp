#include "camera.hpp"
#include <iostream>
#include <SDL2/SDL.h>

Camera::Camera()
{
    _theta = 0;
    _phi = M_PI/2;
    _PosCam = glm::vec3(1, 1, 1);
    PVise();
    Up();
    Left();
    Orientation();
    _sensi = 0.005;
    _vitesse = 1.0;
}

Camera::Camera(glm::vec3 PosCam)
{
    _theta = 0;
    _phi = M_PI/2;
    _PosCam = PosCam;
    PVise();
    Left();
    Up();
    Orientation();
    _sensi = 0.005;
    _vitesse = 1.0;
}

glm::vec3 Camera::GetPosCam() const{
    return _PosCam;
}

glm::vec3 Camera::GetPVise() const{
    return _PVise;
}

glm::vec3 Camera::GetUp() const{
    return _Up;
}

glm::vec3 Camera::GetLeft() const{
    return _Left;
}

glm::vec3 Camera::GetOrientation() const{
    return _Orientation;
}

void Camera::PosCamX(float a){
    Left();
    _PosCam += a * _Left;
}

void Camera::PosCamY(float a){
    Up();
    _PosCam += a * _Up;
}

void Camera::PosCamZ(float a){
    Orientation();
    _PosCam += a * _Orientation;
}

void Camera::phi(float a){
    _phi+=a;
}

void Camera::theta(float a){
    _theta+=a;
}

void Camera::PVise(){
    _PVise=glm::vec3(_PosCam.x+cos(_theta)*sin(_phi), _PosCam.y+sin(_theta), _PosCam.z+cos(_theta)*cos(_phi));
}

void Camera::Left(){
    _Left=glm::vec3(sin(_phi+M_PI/2), 0, cos(_phi+M_PI/2));
}

void Camera::Up(){
    _Up = glm::vec3(sin(_theta)*_Left.z-cos(_theta)*cos(_phi)*_Left.y, cos(_theta)*cos(_phi)*_Left.x-cos(_theta)*sin(_phi)*_Left.z, cos(_theta)*sin(_phi)*_Left.y-sin(_theta)*_Left.x);
}

void Camera::Orientation(){
    _Orientation = glm::vec3(cos(_theta)*sin(_phi), sin(_theta), cos(_theta)*cos(_phi));
}

void Camera::modifVector(){
    PVise();
    Left();
    Up();
    Orientation();
}

void Camera::zoom(int scroll){
    if (scroll<0){
        PosCamZ(-_vitesse);
    }
    else{
        PosCamZ(_vitesse);
    }
}

void Camera::orienter(int xRel, int yRel)
{
    _phi -= _sensi*xRel;
    _theta -= _sensi*yRel;
}

void Camera::deplacer(Input const &input){

    if(input.getTouche(SDL_SCANCODE_D)){
        PosCamX(-_vitesse);
    }

    if(input.getTouche(SDL_SCANCODE_A)){
        PosCamX(_vitesse);
    }

    if(input.getTouche(SDL_SCANCODE_W)){
        PosCamY(_vitesse);
    }

    if(input.getTouche(SDL_SCANCODE_S)){
        PosCamY(-_vitesse);
    }

    if(input.molette()){
        zoom(input.getMolette());
    }

    if(input.getTouche(SDL_SCANCODE_UP)){
        theta(_vitesse*0.1);
    }

    if(input.getTouche(SDL_SCANCODE_DOWN)){
        theta(-_vitesse*0.1);
    }

    if(input.getTouche(SDL_SCANCODE_RIGHT)){
        phi(-_vitesse*0.1);
    }

    if(input.getTouche(SDL_SCANCODE_LEFT)){
        phi(_vitesse*0.1);
    }

    if(input.mouvementSouris()){
        orienter(input.getXRel(), input.getYRel());
        
    }
    
    modifVector();
}



Camera::~Camera()
{}