#include "includeMain.hpp"


int main(int argc, char** argv) {
    Input input; 

    Window fenetre;
    if(!fenetre.init()){
        return EXIT_FAILURE;
    }
    while(input.GetJouer()!=decision::quitter){
        // int a=0;   
        // std::cout<<"test : "<<a<<std::endl;
        // a++;


        //float positionCamX=genererParcours();
        std::string const infos2("../assets/files/infos.txt");
        std::ofstream monFluxInfos(infos2.c_str());

        std::random_device generator;
        std::uniform_int_distribution<int> distributionLargeur(32,64);
        int largeur = distributionLargeur(generator); // garder que les nombres pairs ?
        monFluxInfos << largeur << std::endl;
        std::uniform_int_distribution<int> distributionLongueur(64,128);
        int longueur = distributionLongueur(generator); // garder que les nombres pairs ?
        monFluxInfos << longueur << std::endl;


        std::string const map2("../assets/files/map.txt");
        std::ofstream monFluxMap(map2.c_str());

        std::uniform_int_distribution<int> distributionPosition(0,largeur-5);
        int position = distributionPosition(generator);
        float positionCamX = position;
        int virage = 0;
        int direction = 0; // 0=tout droit; 1=gauche; 2=droite;
        int k = 0;
        int solObstacles = 1;

        while (k<longueur) // ne pas finir sur un virage
        {
            // std::cout <<"position" << position << std::endl;
            std::uniform_int_distribution<int> distributionVirage(5,15);
            virage = distributionVirage(generator);
            int modifObs = 1;
            // std::cout<<"virage : "<<virage<<std::endl;
            // std::cout<<"direction : "<<direction<<std::endl;
            if(direction==0){
                k+=virage;
                for (int j = 0; j < virage; j++)
                {
                    if(j==virage/2){
                        std::uniform_int_distribution<int> distributionObstacles(0,2);
                        solObstacles = distributionObstacles(generator);
                        if(solObstacles==1) solObstacles=3;
                        else if(solObstacles==2) solObstacles=4;
                    }
                    for (int i = 0; i < largeur; i++)
                    {
                        //std::cout<<"test : "<< j << "-" << i<<std::endl;
                        if (i==position){
                            monFluxMap << "2" << std::endl;
                            monFluxMap << solObstacles << std::endl;
                            monFluxMap << solObstacles << std::endl;
                            monFluxMap << solObstacles << std::endl;
                            monFluxMap << "2" << std::endl;
                            i+=4;
                        }
                        else{
                                monFluxMap << "0" << std::endl;
                        }     
                    }
                    if(j==virage/2) solObstacles=1;
                }
            }
            else if(direction==1){
                k+=5;
                std::uniform_int_distribution<int> distributionObstacles(0,2);
                modifObs = distributionObstacles(generator);
                for (int l = 0; l<5; l++){
                    for (int i = 0; i < largeur; i++)
                    {
                        if (l==0 && i==position-virage){
                            for (int m = 0; m < virage+1; m++)
                            {
                                monFluxMap << "2" << std::endl;
                            }
                            monFluxMap << "1" << std::endl;
                            monFluxMap << "1" << std::endl;
                            monFluxMap << "1" << std::endl;
                            monFluxMap << "2" << std::endl;
                            i+=virage+4;
                        }
                        else if(l>0 && l<4 && i==position-virage){
                            monFluxMap << "2" << std::endl;
                            for (int m = 0; m < virage+3; m++)
                            {
                                if(i==position-(virage/2)){
                                    solObstacles=modifObs;
                                    if(solObstacles==1) solObstacles=3;
                                    else if(solObstacles==2) solObstacles=4;
                                    else solObstacles=0;
                                }
                                monFluxMap << solObstacles << std::endl;
                                if(i==position-(virage/2)) solObstacles=1;
                                i+=1;
                            }
                            monFluxMap << "2" << std::endl;
                            i+=1;
                        }
                        else if(l==4 && i==position-virage){
                            monFluxMap << "2" << std::endl;
                            monFluxMap << "1" << std::endl;
                            monFluxMap << "1" << std::endl;
                            monFluxMap << "1" << std::endl;
                            for (int m = 0; m < virage+1; m++)
                            {
                                monFluxMap << "2" << std::endl;
                            }
                            i+=virage+4;
                        }
                        else{
                            monFluxMap << "0" << std::endl;
                        }     
                        if(i==position-(virage/2)) solObstacles=1;
                    }
                }
            }
            else{
                k+=5;
                std::uniform_int_distribution<int> distributionObstacles(0,2);
                modifObs = distributionObstacles(generator);
                for (int l = 0; l<5; l++){
                    for (int i = 0; i < largeur; i++)
                    {
                        if (l==0 && i==position){
                            monFluxMap << "2" << std::endl;
                            monFluxMap << "1" << std::endl;
                            monFluxMap << "1" << std::endl;
                            monFluxMap << "1" << std::endl;
                            for (int m = 0; m < virage+1; m++)
                            {
                                monFluxMap << "2" << std::endl;
                            }
                            i+=virage+4;
                        }
                        else if(l>0 && l<4 && i==position){
                            monFluxMap << "2" << std::endl;
                            for (int m = 0; m < virage+3; m++)
                            {
                                if(i==position+(virage/2)){
                                    if(modifObs==1) solObstacles=3;
                                    else if(modifObs==2) solObstacles=4;
                                    else solObstacles=0;
                                }
                                monFluxMap << solObstacles << std::endl;
                                if(i==position+(virage/2)) solObstacles=1;
                                i+=1;
                            }
                            monFluxMap << "2" << std::endl;
                            i+=1;
                        }
                        else if(l==4 && i==position){
                            for (int m = 0; m < virage+1; m++)
                            {
                                monFluxMap << "2" << std::endl;
                            }
                            monFluxMap << "1" << std::endl;
                            monFluxMap << "1" << std::endl;
                            monFluxMap << "1" << std::endl;
                            monFluxMap << "2" << std::endl;
                            i+=virage+4;
                        }
                        else{
                            monFluxMap << "0" << std::endl;
                        }     
                        if(i==position+(virage/2)) solObstacles=1;
                    }
                }
            }
            int direcProv=direction;
            if(direction==0){
                if (position>largeur/2){
                    direction=1;
                }
                else {
                    direction=2;
                }
            }
            else{
                direction=0;
            }

            if (direcProv==1){
                position-=virage;
            }
            else if(direcProv==2){
                position+=virage;
            }
        }






        FilePath applicationPath(argv[0]);
        const std::string astronaute = "../assets/objects/Astronaut.obj";
        std::string infos = "../assets/files/infos.txt";
        std::string map = "../assets/files/map.txt";
        std::string imgBouteille= "../assets/objects/Bottle.obj";

        Parcours parcours;
        if(!parcours.Lire(infos,map))
            return EXIT_FAILURE;
        std::unique_ptr<glimac::Image>  mur =glimac::loadImage("../assets/textures/mur.png");
        if (mur==NULL){
            return EXIT_FAILURE;
        }
        std::unique_ptr<glimac::Image>  sol =glimac::loadImage("../assets/textures/sol.png");
        if (sol==NULL){
            return EXIT_FAILURE;
        }
        std::unique_ptr<glimac::Image>  obstacle =glimac::loadImage("../assets/textures/obstacle.png");
        if (obstacle==NULL){
            return EXIT_FAILURE;
        }
        std::unique_ptr<glimac::Image>  arche =glimac::loadImage("../assets/textures/arche.png");
        if (arche==NULL){
            return EXIT_FAILURE;
        }



        Skybox skybox;
        SkyboxProgram skyboxProgram(applicationPath);

        Personnage personnage(astronaute);
        Object3dProgram persoProgram(applicationPath);  

        Object3dProgram bouteilleProgram(applicationPath);    

        Decors murDecors;
        murDecors.creerMur();
        ObjectParcoursProgram murProgram(applicationPath);
        GLuint texMur=murDecors.creerTexture(mur);
        Decors solDecors;
        solDecors.creerSol();
        ObjectParcoursProgram solProgram(applicationPath);
        GLuint texSol=solDecors.creerTexture(sol);
        Decors obstacleDecors;
        obstacleDecors.creerObstacle();
        ObjectParcoursProgram obstacleProgram(applicationPath);
        GLuint texObstacle=obstacleDecors.creerTexture(obstacle);
        Decors archeDecors;
        archeDecors.creerArche();
        ObjectParcoursProgram archeProgram(applicationPath);
        GLuint texArche=archeDecors.creerTexture(arche);

        glm::vec3 PosCam(0.f, 2.f, positionCamX+2.5f);
        Camera cam(PosCam);
        CameraPersonnage camPerso(positionCamX); 

        glm::mat4 ProjMatrix=glm::perspective(glm::radians(70.f),1920.f/1080.f,0.1f,100.f);
        glm::mat4 View;

        Matrix matrixDecors;

        std::vector<std::vector<glm::mat4>> tabModel;
        for (int i=0;i<parcours.Getlongueur();i++){
            std::vector<glm::mat4> inter;
            for(int j=0;j<parcours.Getlargeur();j++){
                glm::mat4 m=glm::translate(matrixDecors.getModel(),glm::vec3(i,0,j));
                inter.push_back(m);
            }
            tabModel.push_back(inter);
            inter.clear();
        }

        Bouteille bouteille(imgBouteille);
        bouteille.placementInitial(positionCamX);
        //Bouteille bouteille("../assets/objects/Bottle.obj");
        std::vector<glm::vec2> coordonneesBouteille;
        std::vector<bool> attrapeBouteille;
        for (int i=0;i<parcours.Getlongueur();i++){
            for(int j=0;j<parcours.Getlargeur();j++){
                if (parcours.IsSol(i,j)&&(aleatoire(1,50)==1)){
                    coordonneesBouteille.push_back(glm::vec2(i,j));
                    attrapeBouteille.push_back(false);
                }
            }
        }


        personnage.placementInitial(positionCamX);

        glEnable(GL_DEPTH_TEST);


        input.capturerPointeur(false);

        unsigned int frameRate (1000 / 50);
        Uint32 debutBoucle(0), finBoucle(0), tempsEcoule(0);

        // SDL_Surface *texte = NULL; //*fond = NULL;
        // SDL_Rect positionRec;
        // TTF_Font *police = NULL;
        // SDL_Color couleurNoire = {0, 0, 0};
        // TTF_Init();
        // SDL_Surface *pSurf = SDL_GetWindowSurface(fenetre.getWindow());
        // police = TTF_OpenFont("../assets/fonts/Roboto/Roboto-Regular.ttf", 65);
        // texte = TTF_RenderText_Blended(police, "Salut", couleurNoire);
        input.setTerminer(false);
        while(!(input.GetTerminer())) {
            // SDL_FillRect(pSurf, NULL, SDL_MapRGB(pSurf->format, 255, 255, 255));
            // positionRec.x = 0;
            // positionRec.y = 0;
            // SDL_BlitSurface(fond, NULL, pSurf, &positionRec); /* Blit du fond */
            // positionRec.x = 60;
            // positionRec.y = 370;
            // SDL_BlitSurface(texte, NULL, pSurf, &positionRec); /* Blit du texte */
            // SDL_UpdateWindowSurface(fenetre.getWindow());        
            
            debutBoucle = SDL_GetTicks();
            
            glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            
            input.updateEvenements();
            //cam.deplacer(input);
            View=glm::lookAt(camPerso.GetPosCam(), camPerso.GetPVise(), camPerso.GetUp());
            bouteilleProgram._Program.use();
            for(size_t i= 0;i<coordonneesBouteille.size();i++){
                bouteille.afficher(coordonneesBouteille[i],View,ProjMatrix,bouteilleProgram);
                attrapeBouteille[i]=personnage.attraper_bouteille(coordonneesBouteille[i],attrapeBouteille[i]);
                if (!attrapeBouteille[i])
                    bouteille.Render();

            }   

            // int ig=personnage.indicesV(parcours).x;
            // int id=personnage.indicesV(parcours).y;
            // int jg=personnage.indicesH(parcours).x;
            // int jd=personnage.indicesH(parcours).y;
            int ig=0;
            int id=parcours.Getlongueur();
            int jg=0;
            int jd=parcours.Getlargeur();

            personnage.perdre(parcours);
            personnage.gagner(parcours);
            if (!personnage.gagne()){
                for (int i=ig;i<id;i++){
                    for(int j=jg;j<jd;j++){
                        //std::cout << camPerso.GetPosCam();
                        View=glm::lookAt(camPerso.GetPosCam(), camPerso.GetPVise(), camPerso.GetUp());
                        matrixDecors.setMatrix(tabModel[i][j],View,ProjMatrix);
                        if (parcours.IsMur(i,j)){
                            murDecors.afficher(murProgram,texMur,matrixDecors);
                        }
                        if (parcours.IsSol(i,j)){
                            solDecors.afficher(solProgram,texSol,matrixDecors);    
                        }
                        if (parcours.IsObstacle(i,j)){
                            obstacleDecors.afficher(obstacleProgram,texObstacle,matrixDecors);
                        }
                        if (parcours.IsArche(i,j)){
                            solDecors.afficher(solProgram,texSol,matrixDecors);
                            archeDecors.afficher(archeProgram,texArche,matrixDecors);
                        }
                    }
                }

            }
            persoProgram._Program.use();
            // std::cout << camPerso.GetPosCam() << std::endl;



            if(!(personnage.perd()))
            {       
                personnage.deplacer(input,parcours,camPerso);
                personnage.avancer();
                personnage.setMatrix(View,ProjMatrix);
                personnage.afficher(persoProgram);
                camPerso=personnage.getCamPerso(camPerso);

            }
            // std::cout << camPerso.GetPosCam() << std::endl;

            
            glDepthFunc(GL_LEQUAL);

            skyboxProgram._Program.use();
            View=glm::mat4(glm::mat3(View));
            glUniformMatrix4fv(skyboxProgram.uMVMatrix,1,GL_FALSE,glm::value_ptr(View));
            glUniformMatrix4fv(skyboxProgram.uProjMatrix,1,GL_FALSE,glm::value_ptr(ProjMatrix));
            skybox.draw();
        
            glDepthFunc(GL_LESS);
            input.setJouer(decision::attente);
            while ((personnage.perd()||personnage.gagne())&&(input.GetJouer()==decision::attente)){
                input.updateEvenements();
                input.testJouer();
                if((input.GetJouer()==decision::jouer)||(input.GetJouer()==decision::quitter)){
                    input.setTerminer(true);
                    personnage.afficheScore(); 
                    break;
                }
            }

            
            fenetre.Swap();

            input.testTerminer();

            finBoucle = SDL_GetTicks();
            tempsEcoule = finBoucle - debutBoucle;

            if(tempsEcoule < frameRate)
                SDL_Delay(frameRate - tempsEcoule);
        }
    }
    // TTF_CloseFont(police);
    // TTF_Quit();
    // SDL_FreeSurface(texte);
    SDL_Quit();
    return EXIT_SUCCESS;
}
