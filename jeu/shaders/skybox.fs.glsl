#version 300 es
precision mediump float;
out vec4 FragColor;

in vec3 texCoords;

uniform samplerCube uTexture;

void main()
{    
    FragColor = texture(uTexture, texCoords);
}