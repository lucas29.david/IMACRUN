#version 300 es
precision mediump float;

in vec2 vFragTexCoords;
in vec3 vFragNormal;
in vec3 vFragPosition;


out vec3 fFragTexture;

void main() {
    fFragTexture = vFragNormal;
};
